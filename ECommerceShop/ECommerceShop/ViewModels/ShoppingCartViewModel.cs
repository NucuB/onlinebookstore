﻿
namespace ECommerceShop.ViewModels
{
    public class ShoppingCartViewModel
    {
        public int NoOfItems { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
