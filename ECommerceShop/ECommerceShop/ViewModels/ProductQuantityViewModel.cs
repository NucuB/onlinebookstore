﻿namespace ECommerceShop.ViewModels
{
    public class ProductQuantityViewModel
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }

        public int Id { get; set; }
    }
}
