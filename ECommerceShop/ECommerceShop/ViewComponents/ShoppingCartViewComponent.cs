﻿using ECommerceShop.Data;
using ECommerceShop.Models;
using ECommerceShop.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerceShop.ViewComponents
{
    public class ShoppingCartViewComponent : ViewComponent
    {
        public readonly ApplicationDbContext _context;
        public readonly UserManager<IdentityUser> _userMananger;
        public ShoppingCartViewModel model = new ShoppingCartViewModel();
        public ShoppingCartViewComponent(ApplicationDbContext context, UserManager<IdentityUser> userMananger)
        {
            _context = context;
            _userMananger = userMananger;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var userId = _userMananger.GetUserId(HttpContext.User);
            var user = await _userMananger.FindByIdAsync(userId) as MyUser;
            if(user != null)
            {
                model.NoOfItems = user.NoOfItems;
                model.TotalPrice = user.TotalPrice;
            }
            return View(model);
        }

    }
}
