﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ECommerceShop.Data;
using ECommerceShop.Models;
using Microsoft.AspNetCore.Identity;
using ECommerceShop.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace ECommerceShop.Controllers
{
    [Authorize]
    public class BooksController : Controller
    {
        private readonly ApplicationDbContext _context;
        public readonly UserManager<IdentityUser> _userManager;
       
        public BooksController(ApplicationDbContext context, UserManager<IdentityUser> _userManager)
        {
            _context = context;
            this._userManager = _userManager;
        }

        // GET: Products
        public async Task<IActionResult> Index(string book)
        {
            if (!String.IsNullOrEmpty(book))
            {
                return View( _context.Books.Where(p => p.Title.Contains(book) || p.Genre.Contains(book)).ToList());
            }
            else
            {
                return View( _context.Books.ToList());
            }

        }

        public async Task<IActionResult> Search(String book)
        {
           
            if (!String.IsNullOrEmpty(book))
            {
                return View(await _context.Books.Where(p => p.Title.Contains(book) || p.Genre.Contains(book)).ToListAsync());
            }
            else
            {
                return View(_context.Books.Include(p => p.Author).Include(p => p.Genre));
            }

        }

        public async Task<IActionResult> SeeMyProducts()
        {
            List<ProductQuantityViewModel> myProducts = new List<ProductQuantityViewModel>();
            var userId = _userManager.GetUserId(HttpContext.User);
            var user = await _userManager.FindByIdAsync(userId) as MyUser;
            var userProducts = _context.UserProducts.Where(up => up.UserId.Equals(userId)).ToList();
            foreach(var userProduct in userProducts)
            {
                foreach(var product in await _context.Books.ToListAsync())
                {
                    if(product.BookId == userProduct.BookId)
                    {
                        ProductQuantityViewModel modelProduct = new ProductQuantityViewModel();
                        modelProduct.Name = product.Title;
                        modelProduct.Price = product.Price;
                        modelProduct.Id = product.BookId;
                        modelProduct.Quantity = userProduct.Quantity;
                        myProducts.Add(modelProduct);
                    }
                }
            }
            return View(myProducts);
        }


        public async Task<IActionResult> AddProductToShoppingCart(decimal Price, int Id)
        {
            UserProduct userProduct = new UserProduct();
            var userId = _userManager.GetUserId(HttpContext.User);
            var user = await _userManager.FindByIdAsync(userId) as MyUser;
            user.NoOfItems++;
            user.TotalPrice += Price;
            var theUserProduct = await _context.UserProducts
                .FirstOrDefaultAsync(up => up.BookId == Id && up.UserId.Equals(userId));
            if (theUserProduct != null)
            {
                theUserProduct.Quantity++;
            }
            else
            {
                userProduct.UserId = userId;
                userProduct.BookId = Id;
                userProduct.Quantity = 1;
                _context.UserProducts.Add(userProduct);
            }
            await _context.SaveChangesAsync();

            List<ProductQuantityViewModel> myProducts = new List<ProductQuantityViewModel>();
            var userProducts = _context.UserProducts.Where(up => up.UserId.Equals(userId)).ToList();
            foreach (var userProduct1 in userProducts)
            {
                foreach (var product in await _context.Books.ToListAsync())
                {
                    if (product.BookId == userProduct1.BookId)
                    {
                        ProductQuantityViewModel modelProduct = new ProductQuantityViewModel();
                        modelProduct.Name = product.Title;
                        modelProduct.Price = product.Price;
                        modelProduct.Id = product.BookId;
                        modelProduct.Quantity = userProduct1.Quantity;
                        myProducts.Add(modelProduct);
                    }
                }
            }
            return RedirectToAction("SeeMyProducts", myProducts);
        }

        public async Task<IActionResult> RemoveProductFromShoppingCart(decimal Price, int Id)
        {
            var userId = _userManager.GetUserId(HttpContext.User);
            var user = await _userManager.FindByIdAsync(userId) as MyUser;
            user.NoOfItems--;
            user.TotalPrice -= Price;
            var theUserProduct = await _context.UserProducts
                .FirstOrDefaultAsync(up => up.BookId == Id && up.UserId.Equals(userId));
            if (theUserProduct != null)
            {
                if(theUserProduct.Quantity == 1)
                {
                    _context.UserProducts.Remove(theUserProduct);
                }
                else { 

                   theUserProduct.Quantity--;
                }
            }
            await _context.SaveChangesAsync();
            List<ProductQuantityViewModel> myProducts = new List<ProductQuantityViewModel>();
            var userProducts = _context.UserProducts.Where(up => up.UserId.Equals(userId)).ToList();
            foreach (var userProduct in userProducts)
            {
                foreach (var product in await _context.Books.ToListAsync())
                {
                    if (product.BookId == userProduct.BookId)
                    {
                        ProductQuantityViewModel modelProduct = new ProductQuantityViewModel();
                        modelProduct.Name = product.Title;
                        modelProduct.Price = product.Price;
                        modelProduct.Id = product.BookId;
                        modelProduct.Quantity = userProduct.Quantity;
                        myProducts.Add(modelProduct);
                    }
                }
            }

            return RedirectToAction("SeeMyProducts", myProducts);
        }


        public async Task<IActionResult> RemoveProduct(decimal Price, int Id)
        {
            var userId = _userManager.GetUserId(HttpContext.User);
            var user = await _userManager.FindByIdAsync(userId) as MyUser;
            var product1 = await _context.Books.FirstOrDefaultAsync(p => p.BookId == Id);
            var userProduct1 = await _context.UserProducts.FirstOrDefaultAsync(up => up.BookId == Id && up.UserId.Equals(userId));
            user.NoOfItems -= userProduct1.Quantity;
            user.TotalPrice -= userProduct1.Quantity * product1.Price;
            var theUserProduct = await _context.UserProducts
                .FirstOrDefaultAsync(up => up.BookId == Id && up.UserId.Equals(userId));
            if (theUserProduct != null)
            {
                _context.UserProducts.Remove(theUserProduct);
            }
           
            await _context.SaveChangesAsync();
            List<ProductQuantityViewModel> myProducts = new List<ProductQuantityViewModel>();
            var userProducts = _context.UserProducts.Where(up => up.UserId.Equals(userId)).ToList();
            foreach (var userProduct in userProducts)
            {
                foreach (var product in await _context.Books.ToListAsync())
                {
                    if (product.BookId == userProduct.BookId)
                    {
                        ProductQuantityViewModel modelProduct = new ProductQuantityViewModel();
                        modelProduct.Name = product.Title;
                        modelProduct.Price = product.Price;
                        modelProduct.Id = product.BookId;
                        modelProduct.Quantity = userProduct.Quantity;
                        myProducts.Add(modelProduct);
                    }
                }
            }

            return RedirectToAction("SeeMyProducts", myProducts);
        }







        public async Task<IActionResult> AddProduct(decimal Price, int Id)
        {
            UserProduct userProduct = new UserProduct();
            var userId = _userManager.GetUserId(HttpContext.User);
            var user = await _userManager.FindByIdAsync(userId) as MyUser;
            user.NoOfItems++;
            user.TotalPrice += Price;
            var theUserProduct = await _context.UserProducts
                .FirstOrDefaultAsync(up => up.BookId == Id && up.UserId.Equals(userId));
            if(theUserProduct != null)
            {
                theUserProduct.Quantity++;
            }
            else
            {
                userProduct.UserId = userId;
                userProduct.BookId = Id;
                userProduct.Quantity = 1;
                _context.UserProducts.Add(userProduct);
            }
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }
        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Books
                .FirstOrDefaultAsync(m => m.BookId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }


        public IActionResult ApplyDiscount()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> ApplyDiscount([Bind("Id")] Book product)
        {
            return View(product);
        }

        // GET: Products/Create
        [Authorize(Roles = "Administrator")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator")]
        public async Task<IActionResult> Create([Bind("Id,Title,Author,Genre,Price")] Book product)
        {
            if (ModelState.IsValid)
            {
                _context.Add(product);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Books.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BookId,Title,Author,Genre,Price")] Book product)
        {
            if (id != product.BookId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.BookId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Books
                .FirstOrDefaultAsync(m => m.BookId == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _context.Books.FindAsync(id);
            _context.Books.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _context.Books.Any(e => e.BookId == id);
        }
    }
}
