﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ECommerceShop.Models
{
    public class Book
    {
       

        public int BookId { get; set; }
        public String Title { get; set; }
        public String Author { get; set; }
        public String Genre { get; set; }
        public decimal Price { get; set; }

        public List<UserProduct> UserProducts { get; set; }
        public Book() { }
    }
}
