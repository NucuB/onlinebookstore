﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace ECommerceShop.Models
{
    public class MyUser : IdentityUser
    {
        public int NoOfItems { get; set; }
        public decimal TotalPrice { get; set; }
        public List<UserProduct> UserProducts { get; set; }

        public MyUser()
        {

        }
    }
}
