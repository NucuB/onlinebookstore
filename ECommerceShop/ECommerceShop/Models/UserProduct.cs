﻿namespace ECommerceShop.Models
{
    public class UserProduct
    {
        public int Id { get; set; }
        public MyUser User { get; set; }
        public Book Book { get; set; }
        public string UserId { get; set; }
        public int BookId { get; set; }
        public int Quantity { get; set; }

        public UserProduct() { }
    }
}