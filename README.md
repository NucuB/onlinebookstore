# README #

## Scope

This application should provide a friendly work-flow in order to buy exposed books.



### Functionalities 

* Register/Login(Roles:Admin/Normal)
* Admin panel(CRUD)
* Normal-User panel(CRUD with authorize system)
* Shopping-Cart(Add additional items to cart, remove items from cart)
* Find book by title/author name/price

### Development 

* Developed in C#, ASP.NET CORE
* Database: Microsoft SQL Server Management(Code-first approach)
